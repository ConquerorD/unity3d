﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// Скрипт, с помощью которого будем строить логику геймплея нашей игры.
public class Game_Controller_Classic : MonoBehaviour {

	public static bool done; // Статическая булевая переменная, которая поможет определить правильно ли нарисована фигура.
	public static bool isGameOver; // Статическая булевая переменная, которая будет вызываться, когда проиграем.
	
	public GameObject cursor; // Физический курсор.
	
	public GameObject resume_button; // Кнопка для продолжения игры.
	public GameObject pause_button; // Кнопка паузы.
	
	public GameObject timer_text; // Таймер.
	public GameObject score_text; // Счетчик набранных очков.
	public GameObject game_Over_Interface; // Интерфейс Game Over'a.
	
	public Image square_Image; // Изображение квадрата.
	public Image triangle_Image; // Изображение треугольника.
	public Image rhombus_Image; // Изображение ромба.
	public Image circle_Image; // Изображение круга.
	public Image star_Image; // Изображение звезды.
	
	public Text square_Text; // Текст к изображению квадрата.
	public Text triangle_Text; // Текст к изображению треугольника.
	public Text rhombus_Text; // Текст к изображению ромба.
	public Text circle_Text; // Текст к изображению круга.
	public Text star_Text; // Текст к изображению звезды.
	
	public Text results_text; // Текст вывода результата после Game Over'a.
	
	private LineRenderer line; // Линия с помощью которой рисуем фигуру.
	private int size = 0; // Размер линии. 
	//После того как отрисуем правильно фигуру, то приравниваем значение к 0 чтобы старая фигура, построенная из линии исчезла.

	void Awake(){
		
		cursor.SetActive (true); // Физический курсор активен.
		
		line = GetComponent<LineRenderer>(); // Возвращаем компонент типа LineRenderer.
		
		resume_button.SetActive (false); // Кнопка "Продолжить" не активна при старте игры.
		pause_button.SetActive (true); // Кнока "Паузы" не активна при старте игры.
		
		timer_text.SetActive (true); // Таймер активен при старте игры.
		score_text.SetActive (true); // Счетчик очков активен при старте игры.
		game_Over_Interface.SetActive (false); // Интерфейс Game Over'a не активен при старте игры.
		
		square_Image.enabled = false; // Изображение квадрата не активно при старте игры.
		square_Text.enabled = false; // Подпись к изображению квадрата при старте игры.
		
		triangle_Image.enabled = false; // Изображение треугольника не активно при старте игры.
		triangle_Text.enabled = false; // Подпись к изображению треугольника при старте игры.
		
		rhombus_Image.enabled = false; // Изображение ромба не активно при старте игры.
		rhombus_Text.enabled = false; // Подпись к изображению ромба при старте игры.
		
		circle_Image.enabled = false; // Изображение круга не активно при старте игры.
		circle_Text.enabled = false; // Подпись к изображению круга при старте игры.
		
		star_Image.enabled = false; // Изображение звезды не активно при старте игры.
		star_Text.enabled = false; // Подпись к изображению звезды при старте игры.
		
		isGameOver = false; // Game Over пока-что не настал. =)
	}
	
	void Update () {
		// Если фигура является квадратом, то вызываем его метод, изображение и подпись становятся активными. 
		if(Type_Figure_Classic.isSquare == true){
			Square_Classic ();
			square_Image.enabled = true;
			square_Text.enabled = true;
		}
		
		// Если фигура является треугольником, то вызываем его метод, изображение и подпись становятся активными. 
		if(Type_Figure_Classic.isTriangle == true){
			Triangle_Classic ();
			triangle_Image.enabled = true;
			triangle_Text.enabled = true;
		}
		
		// Если фигура является ромбом, то вызываем его метод, изображение и подпись становятся активными. 
		if(Type_Figure_Classic.isRhombus == true){
			Rhombus_Classic ();
			rhombus_Image.enabled = true;
			rhombus_Text.enabled = true;
		}
		
		// Если фигура является кругом, то вызываем его метод, изображение и подпись становятся активными. 
		if(Type_Figure_Classic.isCircle == true){
			Circle_Classic ();
			circle_Image.enabled = true;
			circle_Text.enabled = true;
		}
		
		// Если фигура является звездой, то вызываем его метод, изображение и подпись становятся активными. 
		if(Type_Figure_Classic.isStar == true){
			Star_Classic();
			star_Image.enabled = true;
			star_Text.enabled = true;
		}
		
		// Если отрисовали квадрат, то его изображение и подпись станут не активными.
		if(Type_Figure_Classic.isSquare == false){
			
			square_Image.enabled = false;
			square_Text.enabled = false;
		}
		
		// Если отрисовали тругольник, то его изображение и подпись станут не активными.
		if(Type_Figure_Classic.isTriangle == false){
			
			triangle_Image.enabled = false;
			triangle_Text.enabled = false;
		}
		
		// Если отрисовали ромб, то его изображение и подпись станут не активными.
		if(Type_Figure_Classic.isRhombus == false){
			
			rhombus_Image.enabled = false;
			rhombus_Text.enabled = false;
		}
		
		// Если отрисовали круг, то его изображение и подпись станут не активными.
		if(Type_Figure_Classic.isCircle == false){
			
			circle_Image.enabled = false;
			circle_Text.enabled = false;
		}
		
		// Если отрисовали звезду, то его изображение и подпись станут не активными.
		if(Type_Figure_Classic.isStar == false){
			
			star_Image.enabled = false;
			star_Text.enabled = false;
		}
		
		// Если мы проиграли, то:
		if(isGameOver == true){
			game_Over_Interface.SetActive(true); // Активируем интерфейс Game Over'a.
			timer_text.SetActive (false); // Таймер становится не активным.
			score_text.SetActive (false); // Счетчик очков становится не активным.
			
			results_text.text = "Results: " + Score_Manager.score; // Выводим результат игрока на экран.
			line.SetVertexCount(size); // Изменяем размер линии.
			UnityEngine.Cursor.visible = true; // Включим видимость курсора мыши.
			cursor.SetActive(false); // Физический курсор не активен.
		}
	}
	
	// Метод Score, который будет вызываться, когда игрок правильно отрисовал фигуру и выполнил для этого все условия:
	// 1. Отрисовал фигуру, пройдя через все точки (звёзды).
	// 2. Уложился в отведённое для отрисовки данной фигуры время.
	// 3. Количество попыток не ограничено.
	void Score(){
		Score_Manager.score += 1; // Если игрок правильно отрисовал  фигуру, то даем ему за это одно очко.
		done = false; // Сбрасываем значение переменной.
		
		// Збрасываем наши чекеры.
		Stars_Checker.one_checked = false; // 1 - й чекер.
		Stars_Checker.two_checked = false; // 2 - й чекер.
		Stars_Checker.three_checked = false; // 3 - й чекер.
		Stars_Checker.four_checked = false; // 4 - й чекер.
		Stars_Checker.five_checked = false; // 5 - й чекер.
		
		Stars_Checker.six_checked = false; // 6 - й чекер.
		Stars_Checker.seven_checked = false; // 7 - й чекер.
		Stars_Checker.eight_checked = false; // 8 - й чекер.
		Stars_Checker.nine_checked = false; // 9 - й чекер.
		Stars_Checker.ten_checked = false; // 10 - й чекер.
	}
	
	// Метод Квадрата, который будет вызываться в случае, если нам нужно отрисовать эту фигуру.
	void Square_Classic(){
		// Если правильно отрисовали квадрат, то:
		if(done == true && Stars_Checker.one_checked == true && Stars_Checker.two_checked == true && Stars_Checker.three_checked == true && Stars_Checker.four_checked == true){
			
			Score (); // Получаем одно очко, обратившись к методу Score.

			// Если игрок набрал меньше 5-ти очков, то получаем такое время в таймере.
			if(Score_Manager.score <= 5){
		        Timer_Classic.micro_second = 59.0f; // Сбросив таймер микро-секунд.
			    Timer_Classic.second = 7.0f; // Сбросив таймер секунд.
			}

			// Если игрок набрал больше 5-ти очков, но меньше 10-ти, то получаем такое время в таймере.
			if(Score_Manager.score >= 5 && Score_Manager.score <= 10){
				Timer_Classic.micro_second = 59.0f; // Сбросив таймер микро-секунд.
				Timer_Classic.second = 6.0f; // Сбросив таймер секунд.
			}

			// Если игрок набрал больше 10-ти очков, то получаем такое время в таймере.
			if(Score_Manager.score >= 10){
				Timer_Classic.micro_second = 59.0f; // Сбросив таймер микро-секунд.
				Timer_Classic.second = 3.0f; // Сбросив таймер секунд.
			}
			
			Type_Figure_Classic.isSquare = false; // Фигура уже не является квадратом.
			line.SetVertexCount(size); // Изменяем размер линии.
		}
	}
	
	// Метод Треугольника, который будет вызыватся в случае, если нам нужно отрисовать эту фигуру.
	void Triangle_Classic(){
		// Если правильно отрисовали треугольник, то:
		if(done == true && Stars_Checker.one_checked == true && Stars_Checker.two_checked == true && Stars_Checker.three_checked == true){
			
			Score (); // Получаем одно очко, обратившись к методу Score.

			// Если игрок набрал меньше 5-ти очков, то получаем такое время в таймере.
			if(Score_Manager.score <= 5){
			    Timer_Classic.micro_second = 59.0f; // Сбросив таймер микро-секунд.
			    Timer_Classic.second = 6.0f; // Сбросив таймер секунд.
			}

			// Если игрок набрал больше 5-ти очков, но меньше 10-ти, то получаем такое время в таймере.
			if(Score_Manager.score >= 5 && Score_Manager.score <= 10){
				Timer_Classic.micro_second = 59.0f; // Сбросив таймер микро-секунд.
				Timer_Classic.second = 5.0f; // Сбросив таймер секунд.
			}

			// Если игрок набрал больше 10-ти очков, то получаем такое время в таймере.
			if(Score_Manager.score >= 10){
				Timer_Classic.micro_second = 59.0f; // Сбросив таймер микро-секунд.
				Timer_Classic.second = 3.0f; // Сбросив таймер секунд.
			}
			
			Type_Figure_Classic.isTriangle = false; // Фигура уже не является треугольником.
			line.SetVertexCount(size); // Изменяем размер линии.
		}
	}
	
	// Метод Ромба, который будет вызыватся в случае, если нам нужно отрисовать эту фигуру.
	void Rhombus_Classic(){
		// Если правильно отрисовали ромб, то:
		if(done == true && Stars_Checker.one_checked == true && Stars_Checker.two_checked == true && Stars_Checker.three_checked == true && Stars_Checker.four_checked == true){
			
			Score (); // Получаем одно очко, обратившись к методу Score.

			// Если игрок набрал меньше 5-ти очков, то получаем такое время в таймере.
			if(Score_Manager.score <= 5){
			Timer_Classic.micro_second = 59.0f; // Сбросив таймер микро-секунд.
			Timer_Classic.second = 5.0f; // Сбросив таймер секунд.
			}

			// Если игрок набрал больше 5-ти очков, но меньше 10-ти, то получаем такое время в таймере.
			if(Score_Manager.score >= 5 && Score_Manager.score <= 10){
				Timer_Classic.micro_second = 59.0f; // Сбросив таймер микро-секунд.
				Timer_Classic.second = 4.0f; // Сбросив таймер секунд.
			}

			// Если игрок набрал больше 10-ти очков, то получаем такое время в таймере.
			if(Score_Manager.score >= 10){
				Timer_Classic.micro_second = 59.0f; // Сбросив таймер микро-секунд.
				Timer_Classic.second = 3.0f; // Сбросив таймер секунд.
			}
			
			Type_Figure_Classic.isRhombus = false; // Фигура уже не является ромбом.
			line.SetVertexCount(size); // Изменяем размер линии.
		}
	}
	
	// Метод Круга, который будет вызыватся в случае, если нам нужно отрисовать эту фигуру.
	void Circle_Classic(){
		// Если правильно отрисовали круг, то:
		if(done == true && Stars_Checker.one_checked == true && Stars_Checker.two_checked == true && Stars_Checker.three_checked == true && Stars_Checker.four_checked == true && Stars_Checker.five_checked == true && 
		   Stars_Checker.six_checked == true && Stars_Checker.seven_checked == true && Stars_Checker.eight_checked == true && Stars_Checker.nine_checked == true && Stars_Checker.ten_checked == true){
			
			Score (); // Получаем одно очко, обратившись к методу Score.

			// Если игрок набрал меньше 5-ти очков, то получаем такое время в таймере.
			if(Score_Manager.score <= 5){
			Timer_Classic.micro_second = 59.0f; // Сбросив таймер микро-секунд.
			Timer_Classic.second = 4.0f; // Сбросив таймер секунд.
			}

			// Если игрок набрал больше 5-ти очков, но меньше 10-ти, то получаем такое время в таймере.
			if(Score_Manager.score >= 5 && Score_Manager.score <= 10){
				Timer_Classic.micro_second = 59.0f; // Сбросив таймер микро-секунд.
				Timer_Classic.second = 3.0f; // Сбросив таймер секунд.
			}

			// Если игрок набрал больше 10-ти очков, то получаем такое время в таймере.
			if(Score_Manager.score >= 10){
				Timer_Classic.micro_second = 59.0f; // Сбросив таймер микро-секунд.
				Timer_Classic.second = 3.0f; // Сбросив таймер секунд.
			}
			
			Type_Figure_Classic.isCircle = false; // Фигура уже не является кругом.
			line.SetVertexCount(size); // Изменяем размер линии.
		}
	}
	
	// Метод Звезды, который будет вызыватся в случае, если нам нужно отрисовать эту фигуру.
	void Star_Classic(){
		// Если правильно отрисовали звезду, то:
		if (done == true && Stars_Checker.one_checked == true && Stars_Checker.two_checked == true && Stars_Checker.three_checked == true && Stars_Checker.four_checked == true && Stars_Checker.five_checked == true && 
		    Stars_Checker.six_checked == true && Stars_Checker.seven_checked == true && Stars_Checker.eight_checked == true && Stars_Checker.nine_checked == true && Stars_Checker.ten_checked == true) {
			
			Score (); // Получаем одно очко, обратившись к методу Score.

			// Если игрок набрал меньше 5-ти очков, то получаем такое время в таймере.
			if(Score_Manager.score <= 5){
			Timer_Classic.micro_second = 59.0f; // Збросив таймер микро-секунд.
			Timer_Classic.second = 4.0f; // Збросив таймер секунд.
			}

			// Если игрок набрал больше 5-ти очков, но меньше 10-ти, то получаем такое времяв таймере.
			if(Score_Manager.score >= 5 && Score_Manager.score <= 10){
				Timer_Classic.micro_second = 59.0f; // Сбросив таймер микро-секунд.
				Timer_Classic.second = 3.0f; // Сбросив таймер секунд.
			}

			// Если игрок набрал больше 10-ти очков, то получаем такое время в таймере.
			if(Score_Manager.score >= 10){
				Timer_Classic.micro_second = 59.0f; // Сбросив таймер микро-секунд.
				Timer_Classic.second = 3.0f; // Сбросив таймер секунд.
			}
			
			Type_Figure_Classic.isStar = false; // Фигура уже не является звездой.
			line.SetVertexCount (size); // Изменяем размер линии.
		}
	}

	// Метод "Пауза". Вызывается когда нажимаем на кнопку паузы.
	public void Pause(){
		Time.timeScale = 0; // Останавливаем время.
		resume_button.SetActive (true); // Кнопка "Продолжить" активна.
		pause_button.SetActive (false); // Кнопка паузы не активна.
	}
	
	// Метод "Продолжить". Вызывается когда нажимаем на кнопку, которая отвечает за продолжение игры после паузы.
	public void Resume(){
		Time.timeScale = 1.0f; // Время идёт!
		resume_button.SetActive (false); // кнопка "Продолжить" не активна.
		pause_button.SetActive (true); // Кнопка паузы активна.
	}
	
	// Метод "Попробуйте Снова". Вызывается когда мы нажимаем на специальную кнопку после проигрыша.
	public void Retry(){
		Application.LoadLevel (1); // Загружаем наш уровень.
		
		// Сбрасываем наши фигуры, на всякий случай.
		Type_Figure_Classic.isSquare = false; // Квадрат.
		Type_Figure_Classic.isTriangle = false; // Треугольник.
		Type_Figure_Classic.isRhombus = false; // Ромб.
		Type_Figure_Classic.isCircle = false; // Круг.
		Type_Figure_Classic.isStar = false; // Звезда.
	}
	
	// Метод "Главного Меню". Вызывается когда нажимаем на специальную кнопку во время игры.
	public void Main_Menu(){
		Application.LoadLevel (0); //Загружаем уровень главного меню.
	}
	
	// Метод "Выход". Вызывается когда нажимаем на специальную кнопку во время игры.
	public void Exit(){
		Application.Quit (); //Выходим и закрываем приложение.
	}
}
