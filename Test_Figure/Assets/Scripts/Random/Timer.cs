﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

// Скрипт таймера отсчета времени, которое дается игроку отрисовать необходимую фигуру.
public class Timer : MonoBehaviour {
	
	public static float micro_second; // Статическая переменная тип float для отсчета микро-секунд.
	public static float second; // Статическая переменная тип float для отсчета секунд.

	private const int par_micro_second = 30; // Константа типа int.

	Text text_timer; // Компонент Text для нашего таймера.
	
	void Awake () {
		text_timer = GetComponent <Text> (); // Возвращаем компонент типа Text.
		
		micro_second = 59.0f; // Стартовая позиция отсчета микро-секунд.
		second = 3.0f; // Стартовая позиция отсчета секунд.
	}

	void Update () {
			micro_second -= par_micro_second * Time.deltaTime; // Формула отсчёта микро-секунд.

		// Если микро-секунд меньше или равно нулю, то обновляем счётчик микро-секунд и отнимаем одну секунду.
			if(micro_second <= 0){
				micro_second = 60.0f;
				second = second - 1.0f;
			}

		// Если секунд меньше или равно нулю, то выводим Game Over.
			if(second <= 0){
				Game_Controller.isGameOver = true;
			}

		// Отображаем наш таймер через компонент Text.
		text_timer.text = "Timer: " + Mathf.Round(second)+ "  " + Mathf.Round(micro_second);
	}
}
