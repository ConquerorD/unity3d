﻿using UnityEngine;
using System.Collections;

// Скрипт, который будет подгружать фигуры в случайном порядке.
// Это усложнит немного "жизнь" игроку и сделает игру непредсказуемой.
public class Type_Figure : MonoBehaviour {
	
		public static bool isSquare; // Статическая булевая переменная, которая определяет является ли эта фигура квадратом.
		public GameObject Square_Pattern; // Паттерн точек для отрисовки квадрата.
	    private GameObject sq; // Клон паттерна точек квадрата.

	    public static bool isTriangle;  // Статическая булевая переменная, которая определяет является ли эта фигура треугольником. 
	    public GameObject Triangle_Pattern; // Паттерн точек для отрисовки треугольника.
	    private GameObject tr; // Клон паттерна точек треугольника.

	    public static bool isRhombus; // Статическая булевая переменная, которая определяет является ли эта фигура ромбом. 
	    public GameObject Rhombus_Pattern; // Паттерн точек для отрисовки ромба.
	    private GameObject rm; // Клон паттерна точек ромба.

	    public static bool isCircle; // Статическая булевая переменная, которая определяет является ли эта фигура кругом. 
	    public GameObject Circle_Pattern; // Паттерн точек для отрисовки круга.
	    private GameObject cr; // Клон паттерна точек круга.

	    public static bool isStar; // Статическая булевая переменная, которая определяет является ли эта фигура звездой. 
	    public GameObject Star_Pattern; // Паттерн точек для отрисовки звезды.
	    private GameObject st; // Клон паттерна точек звезды.

	public Transform spawn_Figure; // Компонент Transform для спауна фигур.

	public int ID_Figure; // Переменная типа int, которая выступает как идентификатор фигур.
	public static bool next_Figure; // Статическая булевая перменная для вызова запроса отрисковки следующей фигуры.
	
	void Start () {
		next_Figure = true; // Так как при старте игры у нас не будет ни одной фигуры, то сделаем запрос на отрисовку фигуры.
	}

	void Update () {
		//Если next_Figure присвоено значение true, то с помощью рандома и корутина отрисуем одну из фигур в зависимости от идентификатора.
		if(next_Figure == true){
			ID_Figure = Random.Range (1, 6); // Диапазон нашего рандома.
			print (ID_Figure); // Для консоли.
			StartCoroutine(add_Figure()); // Наш корутин.
		}

		// Если мы правильно отрисовали квадрат, пройдя через все точки, то уничтожаем его и делаем запрос на следующую фигуру.
		if(Game_Controller.done == true && Stars_Checker.one_checked == true && Stars_Checker.two_checked == true && Stars_Checker.three_checked == true && Stars_Checker.four_checked == true && isSquare == true){
			Destroy (sq);
			next_Figure = true;
		}

		// Если мы правильно отрисовали треугольник, пройдя через все точки, то уничтожаем его и делаем запрос на следующую фигуру.
		if(Game_Controller.done == true &&  Stars_Checker.one_checked == true && Stars_Checker.two_checked == true && Stars_Checker.three_checked == true && isTriangle == true){
			Destroy (tr);
			next_Figure = true;
		}

		// Если мы правильно отрисовали ромб, пройдя через все точки, то уничтожаем его и делаем запрос на следующую фигуру.
		if(Game_Controller.done == true && Stars_Checker.one_checked == true && Stars_Checker.two_checked == true && Stars_Checker.three_checked == true && Stars_Checker.four_checked == true && isRhombus == true){
			Destroy (rm);
			next_Figure = true;
		}

		// Если мы правильно отрисовали круг, пройдя через все точки, то уничтожаем его и делаем запрос на следующую фигуру.
		if(Game_Controller.done == true && Stars_Checker.one_checked == true && Stars_Checker.two_checked == true && Stars_Checker.three_checked == true && Stars_Checker.four_checked == true && Stars_Checker.five_checked == true && 
		   Stars_Checker.six_checked == true && Stars_Checker.seven_checked == true && Stars_Checker.eight_checked == true && Stars_Checker.nine_checked == true && Stars_Checker.ten_checked == true && isCircle == true){
			Destroy (cr);
			next_Figure = true;
		}

		// Если мы правильно отрисовали звезду, пройдя через все точки, то уничтожаем его и делаем запрос на следующую фигуру.
		if(Game_Controller.done == true && Stars_Checker.one_checked == true && Stars_Checker.two_checked == true && Stars_Checker.three_checked == true && Stars_Checker.four_checked == true && Stars_Checker.five_checked == true && 
		   Stars_Checker.six_checked == true && Stars_Checker.seven_checked == true && Stars_Checker.eight_checked == true && Stars_Checker.nine_checked == true && Stars_Checker.ten_checked == true && isStar == true){
			Destroy (st);
			next_Figure = true;
		}
	}

	// В зависимости от идентификатора, с помощью Instantiate отрисовываем ту или иную фигуру.
	private IEnumerator add_Figure(){

		// Если выпадает 1, то отрисовываем квадрат.
		if(ID_Figure == 1){
			isSquare = true; // Эта фигура является квадратом.
			sq = Instantiate(Square_Pattern, spawn_Figure.position, spawn_Figure.rotation) as GameObject; //Отрисовываем фигуру в необходимой точке.
			next_Figure = false; //Прерываем запрос на отрисовку следующей фигуры.
			yield return null; //Все прочие возвращаемые значения указывают, что нужно продолжить после прохода текущей итерации цикла Update.
		}

		// Если выпадает 2, то отрисовываем треугольник.
		if(ID_Figure == 2){
			isTriangle = true; // Эта фигура является треугольником.
			tr = Instantiate(Triangle_Pattern, spawn_Figure.position, spawn_Figure.rotation) as GameObject; //Отрисовываем фигуру в необходимой точке.
			next_Figure = false; //Прерываем запрос на отрисовку следующей фигуры.
			yield return null; //Все прочие возвращаемые значения указывают, что нужно продолжить после прохода текущей итерации цикла Update.
		}

		// Если выпадает 3, то отрисовываем ромб.
		if(ID_Figure == 3){
			isRhombus = true; // Эта фигура является ромбом.
			rm = Instantiate(Rhombus_Pattern, spawn_Figure.position, spawn_Figure.rotation) as GameObject; //Отрисовываем фигуру в необходимой точке.
			next_Figure = false; //Прерываем запрос на отрисовку следующей фигуры.
			yield return null; //Все прочие возвращаемые значения указывают, что нужно продолжить после прохода текущей итерации цикла Update.
		}

		// Если выпадает 4, то отрисовываем круг.
		if(ID_Figure == 4){
			isCircle = true; // Эта фигура является кругом.
			cr = Instantiate(Circle_Pattern, spawn_Figure.position, spawn_Figure.rotation) as GameObject; //Отрисовываем фигуру в необходимой точке.
			next_Figure = false; //Прерываем запрос на отрисовку следующей фигуры.
			yield return null; //Все прочие возвращаемые значения указывают, что нужно продолжить после прохода текущей итерации цикла Update.
		}

		// Если выпадает 5, то отрисовываем звезду.
		if(ID_Figure == 5){
			isStar = true; // Эта фигура является звездой.
			st = Instantiate(Star_Pattern, spawn_Figure.position, spawn_Figure.rotation) as GameObject; //Отрисовываем фигуру в необходимой точке.
			next_Figure = false; //Прерываем запрос на отрисовку следующей фигуры.
			yield return null; //Все прочие возвращаемые значения указывают, что нужно продолжить после прохода текущей итерации цикла Update.
		}
	}
}
