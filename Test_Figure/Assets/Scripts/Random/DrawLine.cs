using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Скрипт, который позволяет рисовать линию, и соотвественно рисовать наши фигуры.
public class DrawLine : MonoBehaviour {    

	private LineRenderer line; // Линия с помощью которой будем рисовать наши фигуры.   
    private bool isMouse_Pressed; // Булевая переменная, которая будет проверять нажата ли кнопка мыши или нет.    
    private List<Vector3> points_List; // Лист точек нашей линии.   
    private Vector3 mouse_Position; // Позиция нашей мыши.

	private int size = 0; // Размер линии.
   
	// Структура точек линии.
    struct myLine{
        public Vector3 start_Point; // Стартовая точка.       
		public Vector3 end_Point; // Конечная точка.   
      };    
       
    void Awake() {

		// Создание компонента визуализации линии и установим его свойства.
        line = GetComponent<LineRenderer>(); // Возвращаем компонент типа LineRenderer.

        line.SetVertexCount(0); // Начальное количество отрезков линии.     
        line.SetWidth(0.3f,0.3f); // Изначальная ширина линии.                
		line.useWorldSpace = true; // Линия определяется в мировом пространстве.
        isMouse_Pressed = false; // Кнопка мыши не зажата.      
        points_List = new List<Vector3>(); 
    }    
      
    void Update () {  

		// Если левая кнопка мыши зажата и drawing_Line_Event присвоенно значение true, то удаляем старую линию и устонавливаем цвет на бирюбзовый.
        if(Input.GetButtonDown("Fire1") && Star.drawing_Line_Event == true)  {            
           isMouse_Pressed = true;            
           line.SetVertexCount(0);             
		   points_List.RemoveRange(0,points_List.Count);             
		   line.SetColors(Color.cyan, Color.cyan);        
    } 
		// Если правильно отрисован квадрат, то линия принимает зеленый цвет и переменная done принимает значение true.
		else if(Input.GetButtonUp("Fire1") && Trigger_Zone.trigger_Zone_Exit_Event == false &&
		        Type_Figure.isSquare == true && Stars_Checker.one_checked == true && Stars_Checker.two_checked == true && Stars_Checker.three_checked == true && Stars_Checker.four_checked == true) {
            isMouse_Pressed = false; 
			line.SetColors(Color.green, Color.green);
			   Game_Controller.done = true;
    }
		// Если правильно отрисован треугольник, то линия принимает зеленый цвет и переменная done принимает значение true.
		else if(Input.GetButtonUp("Fire1") && Trigger_Zone.trigger_Zone_Exit_Event == false &&
		        Type_Figure.isTriangle == true && Stars_Checker.one_checked == true && Stars_Checker.two_checked == true && Stars_Checker.three_checked == true) {
			isMouse_Pressed = false; 
			line.SetColors(Color.green, Color.green);
			Game_Controller.done = true;
		}
		// Если правильно отрисован ромб, то линия принимает зеленый цвет и переменная done принимает значение true.
		else if(Input.GetButtonUp("Fire1") && Trigger_Zone.trigger_Zone_Exit_Event == false &&
		        Type_Figure.isRhombus == true && Stars_Checker.one_checked == true && Stars_Checker.two_checked == true && Stars_Checker.three_checked == true && Stars_Checker.four_checked == true) {
			isMouse_Pressed = false; 
			line.SetColors(Color.green, Color.green);
			Game_Controller.done = true;
		}
		// Если правильно отрисован круг, то линия принимает зеленый цвет и переменная done принимает значение true.
		else if(Input.GetButtonUp("Fire1") && Trigger_Zone.trigger_Zone_Exit_Event == false &&
		        Type_Figure.isCircle == true && Stars_Checker.one_checked == true && Stars_Checker.two_checked == true && Stars_Checker.three_checked == true && Stars_Checker.four_checked == true && Stars_Checker.five_checked == true && 
		        Stars_Checker.six_checked == true && Stars_Checker.seven_checked == true && Stars_Checker.eight_checked == true && Stars_Checker.nine_checked == true && Stars_Checker.ten_checked == true) {
			isMouse_Pressed = false; 
			line.SetColors(Color.green, Color.green);
			Game_Controller.done = true;
		}
		// Если правильно отрисована звезда, то линия принимает зеленый цвет и переменная done принимает значение true.
		else if(Input.GetButtonUp("Fire1") && Trigger_Zone.trigger_Zone_Exit_Event == false &&
		        Type_Figure.isStar == true && Stars_Checker.one_checked == true && Stars_Checker.two_checked == true && Stars_Checker.three_checked == true && Stars_Checker.four_checked == true && Stars_Checker.five_checked == true && 
		        Stars_Checker.six_checked == true && Stars_Checker.seven_checked == true && Stars_Checker.eight_checked == true && Stars_Checker.nine_checked == true && Stars_Checker.ten_checked == true) {
			isMouse_Pressed = false; 
			line.SetColors(Color.green, Color.green);
			Game_Controller.done = true;
		}
		// Если игрок входит в зону, то цвет линии окрашивается в красный цвет и збрасывается прогресс.
		else if(Trigger_Zone.trigger_Zone_Exit_Event == true){
			isMouse_Pressed = false;                     
			line.SetColors(Color.red, Color.red);

			Game_Controller.done = false;
			Stars_Checker.one_checked = false;
			Stars_Checker.two_checked = false;
			Stars_Checker.three_checked = false;
			Stars_Checker.four_checked = false;
			Stars_Checker.five_checked = false;

			Stars_Checker.six_checked = false;
			Stars_Checker.seven_checked = false;
			Stars_Checker.eight_checked = false;
			Stars_Checker.nine_checked = false;
			Stars_Checker.ten_checked = false;
		}

		// Рисуем линию, когда зажата левая кнопка мыши и мышь движется.
    if(isMouse_Pressed){  
			mouse_Position = Camera.main.ScreenToWorldPoint(Input.mousePosition);             
			mouse_Position.z = 0; 

			if (!points_List.Contains (mouse_Position)){ 
				points_List.Add (mouse_Position);                 
	            line.SetVertexCount (points_List.Count);                 
	            line.SetPosition (points_List.Count - 1, 
	           (Vector3)points_List [points_List.Count - 1]);            
            }        
         }
      }    

	// Делаем проверку столконвения текущей линии с предыдущей.
    private bool isLineCollider(){
        if (points_List.Count < 2)            
        return false;

        int total_Lines = points_List.Count - 1;        
		myLine[] lines = new myLine[total_Lines];        
        
		if (total_Lines > 1){ 
            for (int i = 0; i < total_Lines; i++){ 
                lines [i].start_Point = (Vector3)points_List [i];                 
				lines [i].end_Point = (Vector3)points_List [i + 1];             
			}        
        }

		for (int i = 0; i < total_Lines - 1; i++){  
            myLine current_Line;            
			current_Line.start_Point = (Vector3)points_List [points_List.Count - 2];            
			current_Line.end_Point = (Vector3)points_List [points_List.Count - 1];

            if (isLines_Intersect (lines [i], current_Line))                  
				return true;        
     }

       return false;    
    }    
  
	// Делаем проверку одинаковые ли две точки или нет.
    private bool check_Points (Vector3 pointA, Vector3 pointB) {
        return (pointA.x == pointB.x && pointA.y == pointB.y);     
	}    
     
	// Следующий метод проверяет, пересикаются данные две линии или нет.
    private bool isLines_Intersect (myLine L1, myLine L2){
		if (check_Points (L1.start_Point, L2.start_Point) || check_Points (L1.start_Point, L2.end_Point) || check_Points (L1.end_Point, L2.start_Point) || check_Points (L1.end_Point, L2.end_Point))            
        return false;        
		return((Mathf.Max (L1.start_Point.x, L1.end_Point.x) >= Mathf.Min (L2.start_Point.x, L2.end_Point.x)) &&(Mathf.Max (L2.start_Point.x, L2.end_Point.x) >= Mathf.Min (L1.start_Point.x, L1.end_Point.x)) && (Mathf.Max (L1.start_Point.y, L1.end_Point.y) >= Mathf.Min (L2.start_Point.y, L2.end_Point.y)) &&(Mathf.Max (L2.start_Point.y, L2.end_Point.y) >= Mathf.Min (L1.start_Point.y, L1.end_Point.y)));    
    }
}