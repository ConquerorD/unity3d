﻿using UnityEngine;
using System.Collections;

//Маленький скрипт для запуска уровня игры.
public class Main_Menu : MonoBehaviour {

	void Start(){
		UnityEngine.Cursor.visible = true; // Курсор мыши становится видимым.
	}

	// Метод для загрузки уровня.
	public void Classic(){
		Application.LoadLevel (1);
	}

	// Метод для загрузки уровня.
	public void Random_G(){
		Application.LoadLevel (2);
	}

	// Метода для выхода из игры, если вдруг передумали. =)
	public void Exit(){
		Application.Quit ();
	}
}
