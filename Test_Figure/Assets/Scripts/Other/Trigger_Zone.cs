﻿using UnityEngine;
using System.Collections;

// Скрипт, который необходимый для ограничение пространства рисования фигур.
public class Trigger_Zone : MonoBehaviour {

	private Collider2D collider_Zone; // Компонент Collider2D нашей зоны.

	public static bool trigger_Zone_Exit_Event; // Булевая перменная для проверки - вошел ли игрок в зону.

	void Awake(){
		collider_Zone = GetComponent<Collider2D>(); // Возвращаем компонент типа Collider2D.
	}

	// Если игрок зашёл в зону, то булевая переменная принимает значение true.
	void OnTriggerEnter2D(Collider2D cursor){
		trigger_Zone_Exit_Event = true;
	}

	// Если игрок вышел из зоны или вообще не входил в зону, то булевая переменная принимает значение false.
	void OnTriggerExit2D(Collider2D cursor){
		trigger_Zone_Exit_Event = false;
	}
}
