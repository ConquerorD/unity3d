﻿using UnityEngine;
using System.Collections;

// Скрипт, необходимый для проверки пройденных звезд при рисовании линии.
public class Stars_Checker: MonoBehaviour {

	public int stars_Check_ID; // ID для проверки звезды.

	// Статические булевые переменные для проверки при отрисовки линии коснулся ли игрок звезду.
	// 10 булевых переменных - это максимальное количество звезд.
	public static bool one_checked;
	public static bool two_checked;
	public static bool three_checked;
	public static bool four_checked;
	public static bool five_checked;
	public static bool six_checked;
	public static bool seven_checked;
	public static bool eight_checked;
	public static bool nine_checked;
	public static bool ten_checked;

	// Если игрок курсором входит в триггер, то соотвественно с ID звездой, активируется "чек" звезды,
	// то есть идёт учёт, что игрок коснулся звезды.
	void OnTriggerEnter2D(Collider2D cursor){
		if(stars_Check_ID == 1){
			one_checked = true;
		}

		if(stars_Check_ID == 2){
			two_checked = true;
		}

		if(stars_Check_ID == 3){
			three_checked = true;
		}

		if(stars_Check_ID == 4){
			four_checked = true;
		}

		if(stars_Check_ID == 5){
			five_checked = true;
		}
		
		if(stars_Check_ID == 6){
			six_checked = true;
		}
		
		if(stars_Check_ID == 7){
			seven_checked = true;
		}
		
		if(stars_Check_ID == 8){
			eight_checked = true;
		}

		if(stars_Check_ID == 9){
			nine_checked = true;
		}
		
		if(stars_Check_ID == 10){
			ten_checked = true;
		}
	}
}
