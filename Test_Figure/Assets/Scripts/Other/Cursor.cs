﻿using UnityEngine;
using System.Collections;

// С помощью этого скрипта мы заменим наш курсор мыши на его более физический аналог с коллайдером.
public class Cursor : MonoBehaviour {

	public Camera camera; // Добавим нашу камеру для считывания позиции нашей мыши.

	void Start(){
		UnityEngine.Cursor.visible = false; // Отключим видимость курсора мыши.
	}

	void Update () {
		Vector3 vec = camera.ScreenToWorldPoint (Input.mousePosition); //считаем текущую позицию мыши.
		vec.z = 0.0f; // Присвоим координату z к нулю.
		transform.position = vec; //Прировняем текущую позицию нового курсора к позиции старого курсора.
	}
}
