﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Score_Manager : MonoBehaviour {

	public static ulong score; // Статическая переменная score типа ulong.

	Text score_Text; // Компонент Text для отображения набранных очков.
	
	void Awake () {
		score_Text = GetComponent<Text>(); // Возвращаем компонент типа Text.
		score = 0; // Старотовое значение score.
	}

	void Update () {
		// Отображение набранных очков игроком.
		score_Text.text = "Score: " + score;
	}
}
