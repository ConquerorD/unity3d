﻿using UnityEngine;
using System.Collections;

// Скрипт для проигрывания эффекта точек в виде звезд.
public class Star : MonoBehaviour {

	public int star_Tag; // Переменная типа int для идентификации наших звезд.

	public int speed_Rotation; // Переменная типа int скорости вращения.
	public float smooth; // Переменная типа float для сглаженного перехода.
	private float new_Intensity; // Приватная переменная типа float для передачи новых значений интенсивности света компонента Light.

	public static bool drawing_Line_Event; // Статическая булевая переменная для изменения интенсивности света во время рисования линии.
	public static bool light_Event; // Статическая булевая переменная для изменения интенсивности света других звезд.
	public bool intensity_Channing_Activate; // Булевая переменная для изменения иненсивности света.

	private Light light_Component; // Компонент Light.
	private Collider2D collider_Component; //Компонент Collider2D.
	
	void Start () {

		intensity_Channing_Activate = false; // Переменная принимает значение false.
		light_Event = false; // Переменная принимает значение false.

		collider_Component = GetComponent<Collider2D>(); // Возвращаем компонент типа Collider2D.
		collider_Component.isTrigger = true; // Активируем триггер на коллайдере.

		light_Component = GetComponent<Light>(); // Возвращаем компонент типа Light.
		light_Component.intensity = 0.3f; // Начальное значение Intensity компонента Light.
	}

	void Update () {
		// Вращаем наши звёзды.
		transform.Rotate (new Vector3(0, 0, speed_Rotation) * Time.deltaTime);

		// Если intensity_Channing_Activate принимает значение true, то вызываем метод Intensity_Chaning.
		if(intensity_Channing_Activate == true){
			Intensity_Chaning();
		}
	}

	// Метод с помощью которого изменяем интенсивность света компонента Light при отрисовке линии.
	void Intensity_Chaning(){

		// Если зажимаем левую кнопку мыши, то new_Intensity = 1.1, а light_Event принимает значение true.
		if(Input.GetButtonDown("Fire1")){
			new_Intensity = 1.1f;
			light_Event = true;
		}

		// Если отжимаем левую кнопку мыши и light_Event принимает значение false, то new_Intensity принимает изначальное значение 0.3.
		if(Input.GetButtonUp("Fire1") && light_Event == false){
			new_Intensity = 0.3f;
		}

		// Если звезды имеют необходимый на тег, то при рисовании линии изменяем интенсивность света нашей звезды с помощью Mathf.Lerp.
		if(star_Tag == 1 || star_Tag == 2 || star_Tag == 3 || star_Tag == 4 || star_Tag == 5 || star_Tag == 6 || star_Tag == 7 || star_Tag == 8 || star_Tag == 9 || star_Tag == 10){
		    light_Component.intensity = Mathf.Lerp (light_Component.intensity, new_Intensity, Time.deltaTime * smooth);
		}
	}

	// Если входим в триггер нашим курсором (который идентифицируется тегом "Player"),
	// то intensity_Channing_Activate и drawing_Line_Event принимают значение true.
	void OnTriggerEnter2D(Collider2D other) {
		if(other.GetComponent<Collider2D>().tag == "Player"){
			intensity_Channing_Activate = true;
			drawing_Line_Event = true;

			// Если light_Event принимает значение true, то при риосвании линии изменяем интенсивность света наших последующих звезд с помощью Mathf.Lerp
			if(light_Event == true){
				if(star_Tag == 1 || star_Tag == 2 || star_Tag == 3 || star_Tag == 4 || star_Tag == 5 || star_Tag == 6 || star_Tag == 7 || star_Tag == 8 || star_Tag == 9 || star_Tag == 10){
					new_Intensity = 1.1f;
				    light_Component.intensity = Mathf.Lerp (light_Component.intensity, new_Intensity, Time.deltaTime * smooth);
				}
			}
		}
	}

	// Если выходим из триггера нашим курсором,
	// то intensity_Channing_Activate и drawing_Line_Event принимают значение false.
	void OnTriggerExit2D(Collider2D other) {
		if(other.GetComponent<Collider2D>().tag == "Player"){
			intensity_Channing_Activate = false;
			drawing_Line_Event = false;
		}
	}
}
